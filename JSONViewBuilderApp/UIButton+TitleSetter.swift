//
//  UIButton+TitleSetter.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 30/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func setTitle(_ title:String) {
        self.setTitle(title, for:[])
    }
}
