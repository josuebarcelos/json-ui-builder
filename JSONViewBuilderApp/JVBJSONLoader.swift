//
//  JVBJSONLoader.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 30/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class JVBJSONLoader: NSObject {
    
    static let shared = JVBJSONLoader()
    
    lazy var viewsDictionary:[String: AnyObject]? = {
        do {
            if let file = Bundle.main.url(forResource: "view", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: AnyObject] {
                    return object
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }()
    
    func loadViews(fromURL url:URL, completionHandler:@escaping ((_ isReady: Bool) -> Void)) {
        let dataTask = URLSession(configuration: URLSessionConfiguration.default).dataTask(with: url, completionHandler: { (data, response, error) in
            guard let data = data else {
                print("Error retrieving views data from url \(url.absoluteString)")
                completionHandler(false)
                return
            }
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String: AnyObject] {
                self.viewsDictionary = object
                completionHandler(true)
            }
        })
        
        dataTask.resume()
        
    }
    
}
