//
//  CollectionViewCell.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 31/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class SecondViewCell: JSONBuiltCollectionViewCell {
    
    var titleLabel:UILabel?
    var portraitImageView:UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
