//
//  UIColor+HexString.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 30/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import Foundation
import UIKit

enum Color: String {
    case red
    case black
    case darkGray
    case lightGray
    case white
    case gray
    case green
    case blue
    case cyan
    case yellow
    case magenta
    case orange
    case purple
    case brown
    case clear
    
    var create : UIColor? {
        switch self {
        case .red:
            return UIColor.red
        case .black:
            return UIColor.black
        case .darkGray:
            return UIColor.darkGray
        case .lightGray:
            return UIColor.lightGray
        case .white:
            return UIColor.white
        case .gray:
            return UIColor.gray
        case .green:
            return UIColor.green
        case .blue:
            return UIColor.blue
        case .cyan:
            return UIColor.cyan
        case .yellow:
            return UIColor.yellow
        case .magenta:
            return UIColor.magenta
        case .orange:
            return UIColor.orange
        case .purple:
            return UIColor.purple
        case .brown:
            return UIColor.brown
        case .clear:
            return UIColor.clear
        }
    }
}

extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
