//
//  JSONBuiltViewController.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 30/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class JSONBuiltViewController: UIViewController {
    
    var reuseIdentifier:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    public func setupViews() {
        if let viewsDictionary = JVBJSONLoader.shared.viewsDictionary {
            for v in self.view.subviews {
                v.removeFromSuperview()
            }
            let selfClassName = String(describing: type(of:self))
            
            guard let viewDescriptions = viewsDictionary[selfClassName] as? [String:AnyObject] else {
                fatalError("No items describing current controller views")
            }
            
            self.setupViews(withDictionary: viewDescriptions, into:self.view)
            
        }
        
    }
    
    public func setupViews(fromURL url:URL, completionHandler:@escaping ((Bool, NSError?) -> Void)) {
        JVBJSONLoader.shared.loadViews(fromURL: url) { (isReady) in
            if !isReady {
                completionHandler(isReady, NSError(domain: "JSONViewBuilder.JSONBuiltViewController.setupViews", code: 500, userInfo: nil))
                return
            }
            
            self.setupViews()
            completionHandler(isReady,nil)
        }
        
    }
    
    func instantiateView(forType viewType:UIView.Type, withOptions options:[String: AnyObject]?) -> UIView? {
        switch viewType {
        case is UIButton.Type:
            return UIButton(type: .system)
        case is UILabel.Type:
            let label = UILabel()
            if let textAlignment = options?["textAlignment"] as? String {
                switch textAlignment {
                case "left":
                    label.textAlignment = .left
                    break
                case "center":
                    label.textAlignment = .center
                    break
                case "right":
                    label.textAlignment = .right
                    break
                case "natural":
                    label.textAlignment = .natural
                    break
                case "justified":
                    label.textAlignment = .justified
                    break
                default:
                    break
                }
            }
            return label
        case is UITableView.Type:
            if let v = options {
                let cellViewClassName = v["registerClass"] as! String
                let cellClass = NSClassFromString(cellViewClassName) as! UIView.Type
                self.reuseIdentifier = (v["reuseIdentifier"] as! String)
                var tableViewStyle = UITableViewStyle(rawValue: 0)!
                if let tableViewStyleValue = (v["tableStyle"] as? String) {
                    switch tableViewStyleValue {
                    case "grouped":
                        tableViewStyle = .grouped
                        break
                    case "plain":
                        tableViewStyle = .plain
                        break
                    default:
                        break
                    }
                }
                
                let tableView = UITableView(frame: self.view.frame, style: tableViewStyle)
                tableView.delegate = (self as! UITableViewDelegate)
                tableView.dataSource = (self as! UITableViewDataSource)
                tableView.register(cellClass.self, forCellReuseIdentifier: self.reuseIdentifier!)
                return tableView
            }
            return nil
        case is UICollectionView.Type:
            
            if let v = options {
                let cellViewClassName = v["registerClass"] as! String
                let cellClass = NSClassFromString(cellViewClassName) as! UIView.Type
                self.reuseIdentifier = (v["reuseIdentifier"] as! String)
                
                let layoutClassName = v["layoutClass"] as! String
                let layoutClass = NSClassFromString(layoutClassName) as! UICollectionViewFlowLayout.Type
                let collectionViewLayout = layoutClass.init()
                
                let itemSize = v["itemSize"] as! [String:CGFloat]
                
                let itemWidth = (itemSize["width"] == 0) ? self.view.frame.width : itemSize["width"]
                
                let itemHeight = (itemSize["height"] == 0) ? self.view.frame.height : itemSize["height"]
                
                collectionViewLayout.itemSize = CGSize(width: itemWidth!, height: itemHeight!)
                
                if let sectionInsetDictionary = v["sectionInset"] as? [String:CGFloat] {
                    let sectionInset = UIEdgeInsets(top: sectionInsetDictionary["top"]!, left: sectionInsetDictionary["left"]!, bottom: sectionInsetDictionary["bottom"]!, right: sectionInsetDictionary["right"]!)
                    
                    collectionViewLayout.sectionInset = sectionInset
                }
                
                
                let collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: collectionViewLayout)

                if let scrollDirection = v["scrollDirection"] as? String {
                    switch scrollDirection {
                    case "vertical":
                        collectionViewLayout.scrollDirection = .vertical
                        break
                    case "horizontal":
                        collectionViewLayout.scrollDirection = .horizontal
                        break
                    default:
                        collectionViewLayout.scrollDirection = .vertical
                        break
                    }
                }
                
                if let contentInsetDictionary = v["contentInset"] as? [String:CGFloat] {
                    let contentInset = UIEdgeInsets(top: contentInsetDictionary["top"]!, left: contentInsetDictionary["left"]!, bottom: contentInsetDictionary["bottom"]!, right: contentInsetDictionary["right"]!)
                    
                    collectionView.contentInset = contentInset
                }
                
                collectionView.delegate = (self as! UICollectionViewDelegate)
                collectionView.dataSource = (self as! UICollectionViewDataSource)
                collectionView.register(cellClass.self, forCellWithReuseIdentifier: self.reuseIdentifier!)
                return collectionView
            }
            return nil
        case is UIImageView.Type:
            let imageView = UIImageView()
            if let imageName = options?["imageNamed"] as? String {
                imageView.image = UIImage(named: imageName)
            }
            return imageView
        default:
            return viewType.init()
            
        }
    }
    
    func addEvents(toView instantiatedView:UIView, withViewDescriptionDictionary viewDescriptionDictionary:[String: AnyObject]) {
        
        guard let instantiatedView = instantiatedView as? UIControl else {
            return
        }
        
        if let onClick = viewDescriptionDictionary["onClick"] {
            
            let selector = Selector(onClick as! String)
            instantiatedView.addTarget(self, action: selector, for: .touchUpInside)
            
        }
        
    }
    
    func setupViews(withDictionary viewDescriptions:[String:AnyObject], into superView:UIView) {
        
        let views = viewDescriptions["views"] as! [[String : AnyObject]]
        
        var viewsDict = [String:UIView]()
        
        for v in views {
            print(v)
            let className = v["class"] as! String
            let viewClass = NSClassFromString(className) as! UIView.Type
            let key = v["id"] as! String
            
            
            guard let instantiatedView = instantiateView(forType: viewClass, withOptions: v) else {
                fatalError()
            }
            //assign any local property that this controller have with the same name
            if self.responds(to: Selector(key)) {
                self.setValue(instantiatedView, forKey: key)
            }
            instantiatedView.translatesAutoresizingMaskIntoConstraints = false
            superView.addSubview(instantiatedView)
            
            //add events
            addEvents(toView: instantiatedView, withViewDescriptionDictionary: v)
            
            if let _ = v["views"] {
                setupViews(withDictionary: v, into: instantiatedView)
            }
            
            viewsDict[key] = instantiatedView
            
            if let propertiesArray = v["properties"] as? [[String:Any]] {
                
                for prop in propertiesArray {
                    let k = prop.keys.first!
                    if instantiatedView.responds(to: Selector("set\(k.capitalized):")) {
                        
                        instantiatedView.setValue(prop[k], forKey: k)
                        
                    }
                }
            }
            
            if let backgroundColorString = v["backgroundColor"] as? String {
                if let color = Color(rawValue: backgroundColorString)?.create {
                    instantiatedView.backgroundColor = color
                } else {
                    
                    let colorFromHex = UIColor(hex: backgroundColorString)
                    instantiatedView.backgroundColor = colorFromHex
                    
                }
                
            }
            
            
        }
        
        let horizontalConstraints = viewDescriptions["horizontal"] as! [String]
        var constraints = [NSLayoutConstraint]()
        
        for visualFormat in horizontalConstraints {
            print(visualFormat)
            constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:\(visualFormat)", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
            
        }
        
        let verticalConstraints = viewDescriptions["vertical"] as! [String]
        
        for visualFormat in verticalConstraints {
            print(visualFormat)
            constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:\(visualFormat)", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
            
        }
        
        NSLayoutConstraint.activate(constraints)
    }
}
