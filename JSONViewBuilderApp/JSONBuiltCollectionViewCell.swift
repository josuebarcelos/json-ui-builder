//
//  JSONBuiltCollectionViewCell.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 31/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class JSONBuiltCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        if let viewsDictionary = JVBJSONLoader.shared.viewsDictionary {
            for v in self.subviews {
                v.removeFromSuperview()
            }
            let selfClassName = String(describing: type(of:self))
            
            guard let viewDescriptions = viewsDictionary[selfClassName] as? [String:AnyObject] else {
                fatalError("No items describing current view")
            }
            
            self.setupViews(withDictionary: viewDescriptions, into:self)
            
        }
    }
    
    func instantiateView(forType viewType:UIView.Type, withOptions options:[String: AnyObject]?) -> UIView? {
        switch viewType {
        case is UIButton.Type:
            return UIButton(type: .system)
        case is UILabel.Type:
            let label = UILabel()
            if let textAlignment = options?["textAlignment"] as? String {
                switch textAlignment {
                case "left":
                    label.textAlignment = .left
                    break
                case "center":
                    label.textAlignment = .center
                    break
                case "right":
                    label.textAlignment = .right
                    break
                case "natural":
                    label.textAlignment = .natural
                    break
                case "justified":
                    label.textAlignment = .justified
                    break
                default:
                    break
                }
            }
            return label
        case is UIImageView.Type:
            let imageView = UIImageView()
            if let imageName = options?["imageNamed"] as? String {
                imageView.image = UIImage(named: imageName)
            }
            return imageView
        default:
            return viewType.init()
            
        }
    }
    
    func addEvents(toView instantiatedView:UIView, withViewDescriptionDictionary viewDescriptionDictionary:[String: AnyObject]) {
        
        guard let instantiatedView = instantiatedView as? UIControl else {
            return
        }
        
        if let onClick = viewDescriptionDictionary["onClick"] {
            
            let selector = Selector(onClick as! String)
            instantiatedView.addTarget(self, action: selector, for: .touchUpInside)
            
        }
        
    }
    
    func setupViews(withDictionary viewDescriptions:[String:AnyObject], into superView:UIView) {
        
        let views = viewDescriptions["views"] as! [[String : AnyObject]]
        
        var viewsDict = [String:UIView]()
        
        for v in views {
            print(v)
            let className = v["class"] as! String
            let viewClass = NSClassFromString(className) as! UIView.Type
            let key = v["id"] as! String
            
            
            guard let instantiatedView = instantiateView(forType: viewClass, withOptions: v) else {
                fatalError()
            }
            //assign any local property that this controller have with the same name
            if self.responds(to: Selector(key)) {
                self.setValue(instantiatedView, forKey: key)
            }
            instantiatedView.translatesAutoresizingMaskIntoConstraints = false
            superView.addSubview(instantiatedView)
            
            //add events
            addEvents(toView: instantiatedView, withViewDescriptionDictionary: v)
            
            if let _ = v["views"] {
                setupViews(withDictionary: v, into: instantiatedView)
            }
            
            viewsDict[key] = instantiatedView
            
            if let propertiesArray = v["properties"] as? [[String:Any]] {
                
                for prop in propertiesArray {
                    let k = prop.keys.first!
                    if instantiatedView.responds(to: Selector("set\(k.capitalized):")) {
                        
                        instantiatedView.setValue(prop[k], forKey: k)
                        
                    }
                }
            }
            
            if let backgroundColorString = v["backgroundColor"] as? String {
                if let color = Color(rawValue: backgroundColorString)?.create {
                    instantiatedView.backgroundColor = color
                } else {
                    
                    let colorFromHex = UIColor(hex: backgroundColorString)
                    instantiatedView.backgroundColor = colorFromHex
                    
                }
                
            }
            
            
        }
        
        let horizontalConstraints = viewDescriptions["horizontal"] as! [String]
        var constraints = [NSLayoutConstraint]()
        
        for visualFormat in horizontalConstraints {
            print(visualFormat)
            constraints += NSLayoutConstraint.constraints(withVisualFormat: "H:\(visualFormat)", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
            
        }
        
        let verticalConstraints = viewDescriptions["vertical"] as! [String]
        
        for visualFormat in verticalConstraints {
            print(visualFormat)
            constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:\(visualFormat)", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
            
        }
        
        NSLayoutConstraint.activate(constraints)
    }
    
}
