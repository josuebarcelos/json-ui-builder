//
//  SecondViewController.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 31/08/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class SecondViewController: JSONBuiltViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var collectionData = ["Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello","Hello"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier!, for: indexPath) as! SecondViewCell
        cell.titleLabel?.text = collectionData[indexPath.row]
        if indexPath.row == 1 {
            cell.portraitImageView?.image = UIImage(named: "homer")
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionData.count
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
