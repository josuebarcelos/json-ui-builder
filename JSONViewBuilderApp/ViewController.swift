//
//  ViewController.swift
//  JSONViewBuilderApp
//
//  Created by Josué Barcelos Pereira on 23/06/17.
//  Copyright © 2017 Josué Barcelos Pereira. All rights reserved.
//

import UIKit

class ViewController: JSONBuiltViewController, UITableViewDelegate, UITableViewDataSource {
    
    var titleLabel:UILabel?
    var nameTextView:UITextField?
    var helloButton:UIButton?
    var greetingsTableView:UITableView?
    var greetingsArray:[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.greetingsArray = ["Hello"]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.greetingsTableView?.reloadData()
    }
    
    func helloButtonAction() {
        guard let name = self.nameTextView?.text  else { return }
        let greeting = "Hello \(name)!!!"
        self.titleLabel?.text = greeting
        self.greetingsArray?.append(greeting)
        self.greetingsTableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier!, for: indexPath)
        cell.textLabel?.text = self.greetingsArray![indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showSecondViewController", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.greetingsArray!.count
    }
    
    fileprivate func randomColor() -> UIColor {
        let red = CGFloat(drand48())
        let rgb = arc4random_uniform(4)
        switch rgb {
        case 0:
            return UIColor(red:red, green:0, blue:0, alpha:1)
        case 1:
            return UIColor(red:0, green:red, blue:0, alpha:1)
        case 2:
            return UIColor(red:0, green:0, blue:red, alpha:1)
        default:
            let green = CGFloat(drand48())
            let blue = CGFloat(drand48())
            return UIColor(red:red, green:green, blue:blue, alpha:1)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSecondViewController" {
            (segue.destination as! SecondViewController).collectionData = self.greetingsArray!
        }
    }
    
    
}
